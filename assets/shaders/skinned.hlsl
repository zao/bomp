cbuffer ViewMatrices : register(b0) {
	float4x4 view, proj;
};
struct VSInput {
	uint vid : SV_VertexId;
	uint iid : SV_InstanceId;
};
struct PSInput {
	float4 position : SV_Position;
	float3 normal : Color;
	float2 texcoord : Texcoord;
};
struct VertexPNBiBwU {
	float4 position;
	float3 normal; float pad0;
	int indices[4];
	float4 weights;
	float2 texcoord; float pad1[2];
};
StructuredBuffer<VertexPNBiBwU> verts : register(t0);
StructuredBuffer<float4x4> inst : register(t1);
Texture2D diffuseTex : register(t2);
SamplerState linearSmp : register(s0);

PSInput VSMain(VSInput vs) {
  PSInput ps = (PSInput)0;
  VertexPNBiBwU vtx = verts[vs.vid];
  ps.position = vtx.position;
  ps.normal = vtx.normal;
  float4x4 world = inst[vs.iid];
  ps.position = mul(proj, mul(view, mul(world, ps.position)));
  ps.normal = normalize(mul((float3x3)world, ps.normal));
  ps.texcoord = vtx.texcoord;
  return ps;
}

float4 PSMain(PSInput ps) : SV_Target {
  float3 n = normalize(ps.normal);
  float3 l = normalize(float3(0.0f, 1.0f, 1.0f));
  float I = saturate(dot(n, l));
  float3 c = diffuseTex.Sample(linearSmp, ps.texcoord).rgb;
  return float4(I * c, 1.0f);
}
