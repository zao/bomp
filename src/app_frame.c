#define COBJMACROS
#include "app_frame.h"
#include "GLFW/glfw3.h"
#include "GLFW/glfw3native.h"

#include "math_ops.h"
#include "stb_image.h"
#include <float.h>
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>

#define LV_API_OPENGL 1
#define LV_API_D3D11 2

struct mlist {
	void* ptr;
	size_t element_size;
	size_t element_count;
	size_t max_count;
};

void mlist_init(struct mlist* l, size_t element_size, size_t max_count) {
	l->ptr = VirtualAlloc(NULL, element_size * max_count, MEM_RESERVE, PAGE_READWRITE);
	l->element_size = element_size;
	l->element_count = 0;
	l->max_count = max_count;
}

void mlist_destroy(struct mlist* l) {
	VirtualFree(l->ptr, 0, MEM_RELEASE);
	l->ptr = NULL;
	l->element_size = 0;
	l->element_count = 0;
	l->max_count = 0;
}

void mlist_grow(struct mlist* l, size_t count) {
	char* start_new_address = (char*)l->ptr + l->element_count * l->element_size;
	size_t allocation_size = count * l->element_size;
	VirtualAlloc(start_new_address, allocation_size, MEM_COMMIT, PAGE_READWRITE);
}

struct view_matrices {
	struct float4x4 view, proj;
};

struct refreshable_asset {
	void (*refresher)(void* data);
	void* data;
};

static size_t refreshable_asset_count = 0;
static struct mlist refreshable_asset_list;
static struct refreshable_asset* refreshable_assets = NULL;

static struct lv_app_frame {
	struct GLFWwindow* window;
	struct view_matrices view_matrices;
} app_frame;

#if LV_API == LV_API_OPENGL

struct api_state {};

static void api_apply_window_hints() {}
static void api_init() {}
static void api_teardown() {}
static void api_present() {
	glfwSwapBuffers(app_frame.window);
}

#elif LV_API == LV_API_D3D11

#include <d3d11.h>
#include <d3d11sdklayers.h>
#include <d3dcompiler.h>
#include <dxgi.h>

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"dxguid.lib")

static struct api_state {
	HWND wnd;
	IDXGIFactory* factory;
	ID3D11Device* device;
	ID3D11DeviceContext* ctx;
	IDXGISwapChain* swapChain;
	D3D_FEATURE_LEVEL featureLevel;
	D3D11_VIEWPORT viewport;

	DXGI_SAMPLE_DESC msaa;

	ID3D11RenderTargetView* backbufferRTV;
	ID3D11DepthStencilView* defaultDSV;
	ID3D11Buffer* viewMatricesCB;
	ID3D11DepthStencilState* depthTestWrite;
	ID3D11BlendState* solidBlend;
	ID3D11RasterizerState* defaultRS;

	ID3D11VertexShader* testVS;
	ID3D11PixelShader* testPS;
	ID3D11InputLayout* testIL;

	ID3D11ShaderResourceView* spruceDiffuse;
	ID3D11SamplerState* linearSmp;

	ID3D11RasterizerState* debugLineRS;
	ID3D11DepthStencilState* debugLineDS;
	ID3D11VertexShader* debugLineVS;
	ID3D11PixelShader* debugLinePS;
	ID3D11InputLayout* debugLineIL;
} api;

struct animation_frame {
	uint64_t frame_number;
	struct float4x4 bone_matrix;
};

struct mesh_resources {
	ID3D11ShaderResourceView* srv;
	UINT vertex_count;
	struct float4x4* inv_binds;
	uint32_t bone_count;
	uint32_t frame_count;
	struct animation_frame* frames;
};

static void api_apply_window_hints() {
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
}

struct shader_pair_info {
	int64_t timestamp;
	char* filename;
	ID3D11VertexShader** vs;
	ID3D11PixelShader** ps;
	ID3D11InputLayout** il;
};

#define SAFE_RELEASE(p) if ((p)) { (p)->lpVtbl->Release((p)); (p) = NULL; }

static void refresh_shader_pair(void* data) {
	HRESULT hr = S_OK;
	struct shader_pair_info* spi = data;

	struct _stat64 s = {0};
	_stat64(spi->filename, &s);
	if (spi->timestamp >= s.st_mtime || s.st_size == 0) {
		return;
	}
	
	ID3D11VertexShader* newVS = NULL;
	ID3D11PixelShader* newPS = NULL;
	ID3D11InputLayout* newIL = NULL;

	FILE* fh = fopen(spi->filename, "rb");
	fseek(fh, 0, SEEK_END);
	uint32_t sz = ftell(fh);
	fseek(fh, 0, SEEK_SET);
	char* src = malloc(sz);
	fread(src, 1, sz, fh);
	fclose(fh);
	ID3DBlob* vsBlob = NULL;
	ID3DBlob* errBlob = NULL;
	hr = D3DCompile(src, sz, "skinned.hlsl", NULL, NULL, "VSMain", "vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vsBlob, &errBlob);
	if (FAILED(hr)) {
		static char buf[1<<20] = {0};
		sprintf_s(buf, sizeof(buf), "VS:\n%s\n", (char const*)ID3D10Blob_GetBufferPointer(errBlob));
		printf(buf);
		OutputDebugStringA(buf);
	}
	else {
		hr = ID3D11Device_CreateVertexShader(
			api.device,
			ID3D10Blob_GetBufferPointer(vsBlob),
			ID3D10Blob_GetBufferSize(vsBlob),
			NULL,
			&newVS);
	}
	SAFE_RELEASE(errBlob);
	errBlob = NULL;

	ID3DBlob* psBlob = NULL;
	hr = D3DCompile(src, sz, "skinned.hlsl", NULL, NULL, "PSMain", "ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &psBlob, &errBlob);
	if (FAILED(hr)) {
		static char buf[1<<20] = {0};
		sprintf_s(buf, sizeof(buf), "PS:\n%s\n", (char const*)ID3D10Blob_GetBufferPointer(errBlob));
		printf(buf);
		OutputDebugStringA(buf);
	}
	else {
		hr = ID3D11Device_CreatePixelShader(
			api.device,
			ID3D10Blob_GetBufferPointer(psBlob),
			ID3D10Blob_GetBufferSize(psBlob),
			NULL,
			&newPS);
	}

	free(src);

	if (vsBlob) {
		D3D11_INPUT_ELEMENT_DESC ieds[1] = {0};
		hr = ID3D11Device_CreateInputLayout(api.device,
			ieds,
			0,
			ID3D10Blob_GetBufferPointer(vsBlob),
			ID3D10Blob_GetBufferSize(vsBlob),
			&newIL);
	}
	SAFE_RELEASE(vsBlob);
	SAFE_RELEASE(psBlob);
	SAFE_RELEASE(errBlob);

	if (newVS && newPS && newIL) {
		SAFE_RELEASE(*spi->vs);
		SAFE_RELEASE(*spi->ps);
		SAFE_RELEASE(*spi->il);
		*spi->vs = newVS;
		*spi->ps = newPS;
		*spi->il = newIL;
	}
	else {
		SAFE_RELEASE(newVS);
		SAFE_RELEASE(newPS);
		SAFE_RELEASE(newIL);
	}
	spi->timestamp = s.st_mtime;
}

static void api_init() {
	HRESULT hr = S_OK;

	// Create device
	DWORD createFlags = D3D11_CREATE_DEVICE_DEBUG;
	hr = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL,
		createFlags, NULL, 0, D3D11_SDK_VERSION,
		&api.device, &api.featureLevel, &api.ctx);
	
	// Mask annoying debug warnings and break on errors/corruption
	{
		ID3D11Debug* debug;
		hr = ID3D11Device_QueryInterface(api.device, &IID_ID3D11Debug, &debug);
		if (SUCCEEDED(hr)) {
			ID3D11InfoQueue* iq;
			hr = ID3D11Debug_QueryInterface(debug, &IID_ID3D11InfoQueue, &iq);
			if (SUCCEEDED(hr)) {
				ID3D11InfoQueue_SetBreakOnSeverity(iq, D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
				ID3D11InfoQueue_SetBreakOnSeverity(iq, D3D11_MESSAGE_SEVERITY_ERROR, true);
				D3D11_MESSAGE_ID hide[] = {
					D3D11_MESSAGE_ID_CREATEINPUTLAYOUT_EMPTY_LAYOUT,
				};
				D3D11_INFO_QUEUE_FILTER filter = {0};
				filter.DenyList.NumIDs = _countof(hide);
				filter.DenyList.pIDList = hide;
				ID3D11InfoQueue_AddStorageFilterEntries(iq, &filter);
				ID3D11InfoQueue_Release(iq);
			}
			ID3D11Debug_Release(debug);
		}
	}

	// Find highest MSAA mode
	for (int sampleCount = 1; sampleCount <= 32; ++sampleCount) {
		UINT qualityLevelCount;
		HRESULT hr = ID3D11Device_CheckMultisampleQualityLevels(api.device, DXGI_FORMAT_R8G8B8A8_UNORM, sampleCount, &qualityLevelCount);
		if (SUCCEEDED(hr) && qualityLevelCount > 0) {
			api.msaa.Count = sampleCount;
			api.msaa.Quality = qualityLevelCount - 1;
		}
	}
	{
		// Create swapchain
		api.wnd = glfwGetWin32Window(app_frame.window);
		RECT client;
		GetClientRect(api.wnd, &client);
		hr = CreateDXGIFactory(&IID_IDXGIFactory, &api.factory);
		DXGI_SWAP_CHAIN_DESC scd = {
			.BufferCount = 2,
			.BufferDesc = {
				.Format = DXGI_FORMAT_R8G8B8A8_UNORM,
				.Width = client.right,
				.Height = client.bottom,
			},
			.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT,
			.OutputWindow = api.wnd,
			.SampleDesc = api.msaa,
			.SwapEffect = DXGI_SWAP_EFFECT_DISCARD,
			.Windowed = TRUE,
		};
		hr = IDXGIFactory_CreateSwapChain(api.factory, (IUnknown*)api.device, &scd, &api.swapChain);
		// Setup viewport
		api.viewport = (D3D11_VIEWPORT){
			.MaxDepth = 1.0f,
			.Width = (float)client.right,
			.Height = (float)client.bottom,
		};
		// Create backbuffer RTV
		{
			ID3D11Texture2D* backbuffer;
			hr = IDXGISwapChain_GetBuffer(api.swapChain, 0, &IID_ID3D11Texture2D, &backbuffer);
			hr = ID3D11Device_CreateRenderTargetView(api.device, (ID3D11Resource*)backbuffer, NULL, &api.backbufferRTV);
			ID3D11Texture2D_Release(backbuffer);
		}
		// Create default backbuffer depth buffer and view
		{
			D3D11_TEXTURE2D_DESC drd = {
				.Width = client.right,
				.Height = client.bottom,
				.MipLevels = 1,
				.ArraySize = 1,
				.Format = DXGI_FORMAT_D32_FLOAT,
				.SampleDesc = api.msaa,
				.Usage = D3D11_USAGE_DEFAULT,
				.BindFlags = D3D11_BIND_DEPTH_STENCIL,
			};

			ID3D11Texture2D* depthTexture;
			hr = ID3D11Device_CreateTexture2D(api.device, &drd, NULL, &depthTexture);
			hr = ID3D11Device_CreateDepthStencilView(api.device, (ID3D11Resource*)depthTexture, NULL, &api.defaultDSV);
			ID3D11Texture2D_Release(depthTexture);
		}
	}
	// Create view matrix constant buffer
	{
		D3D11_BUFFER_DESC cbd = {
			.BindFlags = D3D11_BIND_CONSTANT_BUFFER,
			.ByteWidth = sizeof(struct view_matrices),
			.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE,
			.StructureByteStride = 0,
			.Usage = D3D11_USAGE_DYNAMIC,
		};
		hr = ID3D11Device_CreateBuffer(api.device, &cbd, NULL, &api.viewMatricesCB);
	}
	// Create less-test+write depth stencil state
	{
		D3D11_DEPTH_STENCIL_DESC dsd = {
			.DepthEnable = TRUE,
			.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL,
			.DepthFunc = D3D11_COMPARISON_LESS,
			.StencilEnable = FALSE,
			.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK,
			.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK,
			.FrontFace = {
				.StencilFunc = D3D11_COMPARISON_ALWAYS,
				.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
				.StencilPassOp = D3D11_STENCIL_OP_KEEP,
				.StencilFailOp = D3D11_STENCIL_OP_KEEP,
			},
		};
		dsd.BackFace = dsd.FrontFace;
		hr = ID3D11Device_CreateDepthStencilState(api.device, &dsd, &api.depthTestWrite);
	}
	// Create solid blend state
	{
		D3D11_RENDER_TARGET_BLEND_DESC rt = {
			.BlendEnable = FALSE,
			.BlendOp = D3D11_BLEND_OP_ADD,
			.SrcBlend = D3D11_BLEND_ONE,
			.DestBlend = D3D11_BLEND_ZERO,
			.BlendOpAlpha = D3D11_BLEND_OP_ADD,
			.SrcBlendAlpha = D3D11_BLEND_ONE,
			.DestBlendAlpha = D3D11_BLEND_ZERO,
			.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL,
		};
		D3D11_BLEND_DESC bd = {0};
		for (size_t i = 0; i < 8; ++i) {
			bd.RenderTarget[i] = rt;
		}
		hr = ID3D11Device_CreateBlendState(api.device, &bd, &api.solidBlend);
	}
	// Create default rasteriser state
	{
		D3D11_RASTERIZER_DESC rd = {
			.FillMode = D3D11_FILL_SOLID,
			.CullMode = D3D11_CULL_BACK,
			.FrontCounterClockwise = FALSE,
			.DepthBias = 0,
			.SlopeScaledDepthBias = 0.0f,
			.DepthBiasClamp = 0.0f,
			.DepthClipEnable = TRUE,
			.ScissorEnable = FALSE,
			.MultisampleEnable = TRUE,
			.AntialiasedLineEnable = FALSE,
		};
		hr = ID3D11Device_CreateRasterizerState(api.device, &rd, &api.defaultRS);
	}
	// Create solid test mesh shaders
	{
		size_t id = refreshable_asset_count++;
		refreshable_assets = realloc(refreshable_assets, refreshable_asset_count * sizeof(struct refreshable_asset));
		struct refreshable_asset* ra = &refreshable_assets[id];
		ra->refresher = refresh_shader_pair;
		struct shader_pair_info* spi = calloc(1, sizeof(struct shader_pair_info));
		spi->filename = strdup("C:/dev/bomp/assets/shaders/skinned.hlsl");
		spi->vs = &api.testVS;
		spi->ps = &api.testPS;
		spi->il = &api.testIL;
		ra->data = spi;
		ra->refresher(ra->data);
	}
	// Create spruce diffuse texture
	{
		int texWidth = 0, texHeight = 0, texComp = 0;
		uint8_t* data = stbi_load("C:/dev/bomp/assets/meshes/spruce-diffuse.png", &texWidth, &texHeight, &texComp, 4);
		D3D11_TEXTURE2D_DESC td = {
			.ArraySize = 1,
			.BindFlags = D3D11_BIND_SHADER_RESOURCE,
			.Format = DXGI_FORMAT_R8G8B8A8_UNORM,
			.Width = texWidth,
			.Height = texHeight,
			.MipLevels = 1,
			.SampleDesc = { .Count = 1, .Quality = 0 },
			.Usage = D3D11_USAGE_IMMUTABLE,
		};
		ID3D11Texture2D* tex;
		D3D11_SUBRESOURCE_DATA srd = {
			.pSysMem = data,
			.SysMemPitch = texWidth * 4,
		};
		hr = ID3D11Device_CreateTexture2D(api.device, &td, &srd, &tex);
		hr = ID3D11Device_CreateShaderResourceView(api.device, (ID3D11Resource*)tex, NULL, &api.spruceDiffuse);
		ID3D11Texture2D_Release(tex);
		stbi_image_free(data);
	}
	// Create linear sampler
	{
		D3D11_SAMPLER_DESC sd = {
			.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR,
			.AddressU = D3D11_TEXTURE_ADDRESS_WRAP,
			.AddressV = D3D11_TEXTURE_ADDRESS_WRAP,
			.AddressW = D3D11_TEXTURE_ADDRESS_WRAP,
			.MinLOD = -FLT_MAX,
			.MaxLOD = FLT_MAX,
			.MaxAnisotropy = 1,
			.ComparisonFunc = D3D11_COMPARISON_NEVER,
		};
		struct float4 border_color = float4(1.0f, 1.0f, 1.0f, 1.0f);
		memcpy(sd.BorderColor, float4_data(&border_color), sizeof(struct float4));
		hr = ID3D11Device_CreateSamplerState(api.device, &sd, &api.linearSmp);
	}
}

struct VertexPNBiBwU {
	struct float4 position;
	struct float3 normal; float pad0;
	int32_t blend_indices[4];
	struct float4 blend_weights;
	struct float2 texcoord; struct float2 pad1;
};

static struct mesh_resources* api_make_mesh(struct VertexPNBiBwU* vertex_data, uint32_t vertex_count) {
	HRESULT hr = S_OK;
	D3D11_BUFFER_DESC bd = {
		.BindFlags = D3D11_BIND_SHADER_RESOURCE,
		.ByteWidth = vertex_count * sizeof(struct VertexPNBiBwU),
		.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
		.StructureByteStride = (UINT)sizeof(struct VertexPNBiBwU),
		.Usage = D3D11_USAGE_IMMUTABLE,
	};
	ID3D11Buffer* geometry_buffer;
	D3D11_SUBRESOURCE_DATA srd = {
		.pSysMem = vertex_data,
	};
	struct mesh_resources* ret = calloc(1, sizeof(struct mesh_resources));
	hr = ID3D11Device_CreateBuffer(api.device, &bd, &srd, &geometry_buffer);
	hr = ID3D11Device_CreateShaderResourceView(api.device, (ID3D11Resource*)geometry_buffer, NULL, &ret->srv);
	ID3D11Buffer_Release(geometry_buffer);
	ret->vertex_count = vertex_count;
	return ret;
}

static struct mesh_resources* api_make_plane_mesh(struct float3 normal, float width, float height) {
	width /= 2.0f;
	height /= 2.0f;
	struct float4 points[4] = {
		float4(-width, 0.0f, +height, 1.0f), // near left
		float4(-width, 0.0f, -height, 1.0f), // far left
		float4(+width, 0.0f, +height, 1.0f), // near right
		float4(+width, 0.0f, -height, 1.0f), // far right
	};
	struct float2 tcs[4] = {
		{ 0.0f, 0.0f },
		{ 0.0f, 1.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 1.0f },
	};
	struct float3 forward, right;
	float decider = dot3(float3_unit_x(), normal);
	if (fabsf(decider) < 0.2f) {
		forward = float3_normalize(cross(float3_unit_x(), normal));
		right = float3_normalize(cross(normal, forward));
	}
	else {
		forward = float3_normalize(cross(float3_unit_z(), normal));
		right = float3_normalize(cross(normal, forward));
	}
	struct float4x4 m = float4x4_from_columns(float4_from_vector(right), float4_from_vector(normal), float4_from_vector(forward), float4_unit_w());
	for (size_t i = 0; i < 4; ++i) {
		points[i] = float4x4_transform_float4(m, points[i]);
	}
	struct VertexPNBiBwU verts[6] = {
		{ .position = points[0], .normal = normal, .texcoord = tcs[0] },
		{ .position = points[1], .normal = normal, .texcoord = tcs[1] },
		{ .position = points[2], .normal = normal, .texcoord = tcs[2] },
		{ .position = points[2], .normal = normal, .texcoord = tcs[2] },
		{ .position = points[1], .normal = normal, .texcoord = tcs[1] },
		{ .position = points[3], .normal = normal, .texcoord = tcs[3] },
	};
	return api_make_mesh(verts, 6);
}

static struct mesh_resources* api_load_mesh(char const* filename) {
	FILE* fh;
	fopen_s(&fh, filename, "rb");
	uint32_t vertex_count = 0;
	fread(&vertex_count, sizeof(uint32_t), 1, fh);

	struct VertexPNBiBwU* verts = malloc(vertex_count * sizeof(struct VertexPNBiBwU));
	fread(verts, sizeof(struct VertexPNBiBwU), vertex_count, fh);

#if 0
	uint32_t bone_count = 0;
	fread(&bone_count, sizeof(uint32_t), 1, fh);
	struct float4x4* inv_binds = malloc(bone_count * sizeof(struct float4x4));
	fread(inv_binds, sizeof(struct float4x4), bone_count, fh);

	uint32_t frame_count = 0;
	fread(&frame_count, sizeof(uint32_t), 1, fh);
	struct animation_frame* frames = malloc(bone_count * frame_count * sizeof(struct animation_frame));
	fread(frames, sizeof(struct animation_frame), bone_count * frame_count, fh);
#endif
	fclose(fh);

	struct mesh_resources* ret = api_make_mesh(verts, vertex_count);
	free(verts);

#if 0
	ret->inv_binds = inv_binds;
	ret->bone_count = bone_count;
	ret->frame_count = frame_count;
	ret->frames = frames;
#endif

	return ret;
}

void api_destroy_mesh(struct mesh_resources* mesh) {
	if (mesh) {
#if 0
		free(mesh->frames);
		free(mesh->inv_binds);
#endif
		SAFE_RELEASE(mesh->srv);
		free(mesh);
	}
}

static void api_teardown() {
	IDXGIFactory_Release(api.factory);
	ID3D11Device_Release(api.device);
	ID3D11DeviceContext_Release(api.ctx);
	IDXGISwapChain_Release(api.swapChain);
	ID3D11RenderTargetView_Release(api.backbufferRTV);
	ID3D11DepthStencilView_Release(api.defaultDSV);
	ID3D11Buffer_Release(api.viewMatricesCB);
	ID3D11DepthStencilState_Release(api.depthTestWrite);
	ID3D11BlendState_Release(api.solidBlend);
	ID3D11RasterizerState_Release(api.defaultRS);
	SAFE_RELEASE(api.testVS);
	SAFE_RELEASE(api.testPS);
	SAFE_RELEASE(api.testIL);
	ID3D11ShaderResourceView_Release(api.spruceDiffuse);
	ID3D11SamplerState_Release(api.linearSmp);
}

static void api_present() {
	IDXGISwapChain_Present(api.swapChain, 0, 0);
}

static void api_clear_target(struct target* target, struct float4 clear_color) {
	ID3D11DeviceContext_ClearRenderTargetView(api.ctx, api.backbufferRTV, float4_data(&clear_color));
	ID3D11DeviceContext_ClearDepthStencilView(api.ctx, api.defaultDSV, D3D11_CLEAR_DEPTH, 1.0f, D3D11_DEFAULT_STENCIL_REFERENCE);
}

#endif

static void on_key(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

struct object {
	mesh_id mesh;
	struct float4x4 transform;
};

static object_id object_id_source = 0;

static struct mlist object_list;
static struct object* objects = NULL;

static mesh_id mesh_id_source = 0;

static struct mlist mesh_name_list;
static char** mesh_names = NULL;

static struct mlist mesh_list;
static struct mesh_resources** meshes = NULL;

void lv_app_frame_init(uint16_t width, uint16_t height, enum LV_APP_FRAME_FLAGS flags) {
	glfwInit();
	glfwWindowHint(GLFW_DECORATED, GL_FALSE);
	int monitorCount = 0;
	GLFWvidmode const* bestMode = NULL;
	GLFWmonitor* bestMonitor = NULL;
	GLFWmonitor** monitors = glfwGetMonitors(&monitorCount);
	for (int i = 0; i < monitorCount; ++i) {
		GLFWvidmode const* mode = glfwGetVideoMode(monitors[i]);
		if (!bestMode || bestMode->width < mode->width) {
			bestMonitor = monitors[i];
			bestMode = mode;
		}
	}
	glfwWindowHint(GLFW_RED_BITS, bestMode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, bestMode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, bestMode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, bestMode->refreshRate);
	glfwWindowHint(GLFW_AUTO_ICONIFY, GL_FALSE);
	api_apply_window_hints();
	app_frame.window = glfwCreateWindow(bestMode->width, bestMode->height, "bomp", bestMonitor, NULL);
	glfwSetKeyCallback(app_frame.window, &on_key);

	//glfwSetInputMode(appFrame.window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	api_init();

	mlist_init(&object_list, sizeof(struct object), 1ULL<<32);
	objects = object_list.ptr;
	mlist_init(&mesh_name_list, sizeof(char*), 1ULL<<32);
	mesh_names = mesh_name_list.ptr;
	mlist_init(&mesh_list, sizeof(struct mesh_resources*), 1ULL<<32);
	meshes = mesh_list.ptr;
}

void lv_app_frame_teardown() {
	for (size_t i = 0; i < mesh_id_source; ++i) {
		free(mesh_names[i]);
		api_destroy_mesh(meshes[i]);
	}
	mesh_id_source = 0;
	api_teardown();
	glfwTerminate();
}

bool lv_app_running() {
	return glfwWindowShouldClose(app_frame.window) == GL_FALSE;
}

void lv_app_pump_events() {
	glfwPollEvents();
}

void lv_app_present() {
	api_present();
}

mesh_id lv_make_plane_mesh(struct float3 normal, float width, float height) {
	mesh_id new_id = mesh_id_source++;
	char name[2048];
	sprintf(name, "*plane*%" PRIu64, new_id);

	mlist_grow(&mesh_list, 1);
	mlist_grow(&mesh_name_list, 1);

	mesh_names[new_id] = strdup(name);
	meshes[new_id] = api_make_plane_mesh(normal, width, height);
	return new_id;
}

struct mesh_info {
	int64_t timestamp;
	char* filename;
	struct mesh_resources** mesh;
};

static void refresh_mesh(void* data) {
	struct mesh_info* mi = data;
	struct _stat64 s = {0};
	_stat64(mi->filename, &s);
	if (mi->timestamp < s.st_mtime) {
		struct mesh_resources* newMesh = api_load_mesh(mi->filename);
		if (newMesh) {
			api_destroy_mesh(*mi->mesh);
			*mi->mesh = newMesh;
		}
		mi->timestamp = s.st_mtime;
	}
}

mesh_id lv_load_mesh(char const* filename) {
	for (int i = 0; i < mesh_id_source; ++i) {
		if (strcmp(mesh_names[i], filename) == 0) {
			return i;
		}
	}
	char filepath[1024] = "C:/dev/bomp/";
	strcat_s(filepath, sizeof(filepath), filename);

	mlist_grow(&mesh_list, 1);
	mlist_grow(&mesh_name_list, 1);

	mesh_id new_id = mesh_id_source++;
	mesh_names[new_id] = strdup(filepath);
	meshes[new_id] = NULL;
	
	struct mesh_info* mi = calloc(1, sizeof(struct mesh_info));
	mi->filename = strdup(filepath);
	mi->mesh = &meshes[new_id];
	size_t r_id = refreshable_asset_count++;
	refreshable_assets = realloc(refreshable_assets, refreshable_asset_count * sizeof(struct refreshable_asset));
	struct refreshable_asset* ra = &refreshable_assets[r_id];
	ra->data = mi;
	ra->refresher = refresh_mesh;
	ra->refresher(ra->data);
	return new_id;
}

object_id lv_create_object() {
	object_id new_id = object_id_source++;

	mlist_grow(&object_list, 1);

	objects[new_id].mesh = 0;
	objects[new_id].transform = float4x4_identity();
	return new_id;
}

void lv_create_objects(object_id* out, size_t count) {
	mlist_grow(&object_list, count);

	while (count > 0) {
		object_id new_id = object_id_source++;
		*out++ = new_id;
		objects[new_id].mesh = 0;
		objects[new_id].transform = float4x4_identity();
		--count;
	}
}

void lv_set_object_mesh(object_id object, mesh_id mesh) {
	objects[object].mesh = mesh;
}

void lv_set_object_transform(object_id object, struct float4x4 transform) {
	objects[object].transform = transform;
}

void lv_clear_target(struct target* target, struct float4 clearColor) {
	api_clear_target(target, clearColor);
}

void lv_set_view_matrices(struct float4x4 view_matrix, struct float4x4 projection_matrix) {
	app_frame.view_matrices.view = view_matrix;
	app_frame.view_matrices.proj = projection_matrix;
}

float lv_get_time() {
	return (float)glfwGetTime();
}

#if LV_API == LV_API_OPENGL

void lv_draw() {}

#elif LV_API == LV_API_D3D11

void lv_draw() {
	if (!api.testVS || !api.testPS || !api.testIL) {
		return;
	}
	HRESULT hr = S_OK;
	{
		D3D11_MAPPED_SUBRESOURCE msr = {0};
		ID3D11DeviceContext_Map(api.ctx, (ID3D11Resource*)api.viewMatricesCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
		memcpy(msr.pData, &app_frame.view_matrices, sizeof(struct view_matrices));
		ID3D11DeviceContext_Unmap(api.ctx, (ID3D11Resource*)api.viewMatricesCB, 0);
	}
	size_t* bucket_sizes = calloc(mesh_id_source, sizeof(size_t));
	struct float4x4** buckets = calloc(mesh_id_source, sizeof(struct float4x4*));
	for (size_t i = 0; i < object_id_source; ++i) {
		mesh_id mesh = objects[i].mesh;
		size_t new_idx = bucket_sizes[mesh];
		++bucket_sizes[mesh];
		buckets[mesh] = realloc(buckets[mesh], bucket_sizes[mesh] * sizeof(struct float4x4));
		buckets[mesh][new_idx] = objects[i].transform;
	}
	for (size_t i = 0; i < mesh_id_source; ++i) {
		size_t count = bucket_sizes[i];
		if (count == 0) continue;
		struct mesh_resources* mesh = meshes[i];
		if (!mesh) continue;
		struct float4x4* instances = buckets[i];
		ID3D11ShaderResourceView* instanceSRV;
		// Make instance buffer
		{
			ID3D11Buffer* instanceBuffer;
			D3D11_BUFFER_DESC bd = {
				.BindFlags = D3D11_BIND_SHADER_RESOURCE,
				.ByteWidth = (UINT)(count * sizeof(struct float4x4)),
				.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
				.StructureByteStride = sizeof(struct float4x4),
				.Usage = D3D11_USAGE_IMMUTABLE,
			};
			D3D11_SUBRESOURCE_DATA srd = {
				.pSysMem = instances,
			};
			hr = ID3D11Device_CreateBuffer(api.device, &bd, &srd, &instanceBuffer);
			hr = ID3D11Device_CreateShaderResourceView(api.device, (ID3D11Resource*)instanceBuffer, NULL, &instanceSRV);
			ID3D11Buffer_Release(instanceBuffer);
		}
		ID3D11ShaderResourceView* srvs[2] = {
			mesh->srv, instanceSRV,
		};
		ID3D11DeviceContext_VSSetShaderResources(api.ctx, 0, 2, srvs);
		ID3D11ShaderResourceView_Release(instanceSRV);

		ID3D11DeviceContext_PSSetSamplers(api.ctx, 0, 1, &api.linearSmp);
		ID3D11DeviceContext_PSSetShaderResources(api.ctx, 2, 1, &api.spruceDiffuse);

		ID3D11DeviceContext_RSSetState(api.ctx, api.defaultRS);
		ID3D11DeviceContext_OMSetRenderTargets(api.ctx, 1, &api.backbufferRTV, api.defaultDSV);
		ID3D11DeviceContext_OMSetDepthStencilState(api.ctx, api.depthTestWrite, D3D11_DEFAULT_STENCIL_REFERENCE);
		struct float4 zero = float4_zero();
		ID3D11DeviceContext_OMSetBlendState(api.ctx, api.solidBlend, float4_data(&zero), D3D11_DEFAULT_SAMPLE_MASK);
		ID3D11DeviceContext_VSSetConstantBuffers(api.ctx, 0, 1, &api.viewMatricesCB);
		ID3D11DeviceContext_RSSetViewports(api.ctx, 1, &api.viewport);
		ID3D11DeviceContext_IASetPrimitiveTopology(api.ctx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		ID3D11DeviceContext_VSSetShader(api.ctx, api.testVS, NULL, 0);
		ID3D11DeviceContext_PSSetShader(api.ctx, api.testPS, NULL, 0);
		ID3D11DeviceContext_IASetInputLayout(api.ctx, api.testIL);

		ID3D11DeviceContext_DrawInstanced(api.ctx, mesh->vertex_count, (UINT)count, 0, 0);
	}

	ID3D11Buffer* cb0[1] = {0};
	ID3D11DeviceContext_VSSetConstantBuffers(api.ctx, 0, 1, cb0);
	ID3D11DeviceContext_VSSetShader(api.ctx, NULL, NULL, 0);
	ID3D11DeviceContext_PSSetShader(api.ctx, NULL, NULL, 0);

	free(bucket_sizes);
	for (size_t i = 0; i < mesh_id_source; ++i) {
		free(buckets[i]);
	}
	free(buckets);
}

void lv_refresh_assets() {
	for (int i = 0; i < refreshable_asset_count; ++i) {
		struct refreshable_asset* ra = &refreshable_assets[i];
		ra->refresher(ra->data);
	}
}

#endif