#pragma once
#include <stdint.h>

#include "math_types.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

enum LV_APP_FRAME_FLAGS {
	LV_APP_FRAME_FLAGS_WINDOWED = 1
};

void lv_app_frame_init(uint16_t width, uint16_t height, enum LV_APP_FRAME_FLAGS flags);
void lv_app_frame_teardown();

bool lv_app_running();
void lv_app_pump_events();
void lv_app_present();

typedef struct target target;

typedef uint64_t mesh_id;
typedef uint64_t object_id;

mesh_id lv_make_plane_mesh(struct float3 normal, float width, float height);
mesh_id lv_load_mesh(char const* filename);

void lv_refresh_assets();

object_id lv_create_object();
void lv_create_objects(object_id* ids, size_t count);
void lv_set_object_mesh(object_id object, mesh_id mesh);
void lv_set_object_transform(object_id object, struct float4x4 transform);

float lv_get_time();
void lv_clear_target(struct target* target, struct float4 clear_color);
void lv_set_view_matrices(struct float4x4 view_matrix, struct float4x4 projection_matrix);
void lv_draw();

#ifdef __cplusplus
}
#endif