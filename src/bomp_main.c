#include "app_frame.h"

#include "math_types.h"
#include "math_ops.h"

#include <stdlib.h>
#include <stdio.h>

struct Line {
	struct float3 from;
	struct float3 to;
};

struct Line* lines;

int main() {
	lv_app_frame_init(1920, 1080, LV_APP_FRAME_FLAGS_WINDOWED);

	mesh_id groundMesh = lv_make_plane_mesh(float3_unit_y(), 10.0f, 10.0f);
	mesh_id testMesh = lv_load_mesh("assets/meshes/human.lv_mesh");
	mesh_id spruceMesh = lv_load_mesh("assets/meshes/spruce.lv_mesh");
	object_id trees[3] = {0};
	object_id blokes[2] = {0};
	lv_create_objects(trees, 3);
	lv_create_objects(blokes, 2);
	for (size_t i = 0; i < 3; ++i) {
		object_id o = trees[i];
		lv_set_object_mesh(o, spruceMesh);
	}
	for (size_t i = 0; i < 2; ++i) {
		object_id o = blokes[i];
		lv_set_object_mesh(o, testMesh);
	}
	object_id groundPlane = lv_create_object();
	lv_set_object_mesh(groundPlane, groundMesh);

	float lastRefreshTime = 0.0f;

	while (lv_app_running()) {
		lv_app_pump_events();

		float t = lv_get_time();
		lv_set_object_transform(trees[0], float4x4_mul(
			float4x4_translate( 0.0f, sinf(3.0f * t) * 0.3f - 0.3f,  0.0f),
			float4x4_rotate_y(t + 0.10f)));
		lv_set_object_transform(trees[1], float4x4_mul(
			float4x4_translate( 3.0f, sinf(4.0f * t + 0.2f) * 0.3f - 0.3f,  0.0f),
			float4x4_rotate_y(t + 0.32f)));
		lv_set_object_transform(trees[2], float4x4_mul(
			float4x4_translate(-2.0f, sinf(2.0f * t + 1.3f) * 0.3f - 0.3f,  1.0f),
			float4x4_rotate_y(t + 0.30f)));

		lv_set_object_transform(blokes[0], float4x4_mul(float4x4_translate( 1.0f, 0.0f, 2.0f), float4x4_rotate_y(0.50f)));
		lv_set_object_transform(blokes[1], float4x4_mul(float4x4_translate(-2.0f, 0.0f, 3.0f), float4x4_rotate_y(-0.6f)));

		lv_clear_target(NULL, float4(0.1f, 0.2f, 0.3f, 1.0f));

		float aspectRatio = 16.0f / 9.0f;
		struct float4x4 view = float4x4_mul(
			float4x4_translate(0.0f, -2.0f, -15.0f),
			float4x4_rotate_x(0.4f));
		struct float4x4 proj = float4x4_perspective_d3d_rh(0.1f, 100.0f, 0.1f * aspectRatio, 0.1f);
		lv_set_view_matrices(view, proj);

		if (lastRefreshTime <= t - 0.1f) {
			lv_refresh_assets();
			lastRefreshTime = t;
		}

		lv_draw();

		lv_app_present();
	}
	lv_app_frame_teardown();
}