#pragma once

#include <math.h>
#include "math_types.h"

#ifdef __cplusplus
extern "C" {
#endif

inline struct float3 float3_zero() {
	struct float3 ret = { 0.0f, 0.0f, 0.0f };
	return ret;
}

inline struct float3 float3_unit_x() {
	struct float3 ret = { 1.0f, 0.0f, 0.0f };
	return ret;
}

inline struct float3 float3_unit_y() {
	struct float3 ret = { 0.0f, 1.0f, 0.0f };
	return ret;
}

inline struct float3 float3_unit_z() {
	struct float3 ret = { 0.0f, 0.0f, 1.0f };
	return ret;
}

inline struct float3 float3(float x, float y, float z) {
	struct float3 ret = { x, y, z };
	return ret;
}

inline struct float4 float4_zero() {
	struct float4 ret = { 0.0f, 0.0f, 0.0f, 0.0f };
	return ret;
}

inline struct float4 float4_unit_x() {
	struct float4 ret = { 1.0f, 0.0f, 0.0f, 0.0f };
	return ret;
}

inline struct float4 float4_unit_y() {
	struct float4 ret = { 0.0f, 1.0f, 0.0f, 0.0f };
	return ret;
}

inline struct float4 float4_unit_z() {
	struct float4 ret = { 0.0f, 0.0f, 1.0f, 0.0f };
	return ret;
}

inline struct float4 float4_unit_w() {
	struct float4 ret = { 0.0f, 0.0f, 0.0f, 1.0f };
	return ret;
}

inline struct float4 float4(float x, float y, float z, float w) {
	struct float4 ret = { x, y, z, w };
	return ret;
}

inline struct float4 float4_from_point(struct float3 xyz) {
	struct float4 ret = {
		xyz.data[0], xyz.data[1], xyz.data[2], 1.0f
	};
	return ret;
}

inline struct float4 float4_from_vector(struct float3 xyz) {
	struct float4 ret = {
		xyz.data[0], xyz.data[1], xyz.data[2], 0.0f
	};
	return ret;
}

inline struct float4 float4_from_float3(struct float3 xyz, float w) {
	struct float4 ret = {
		xyz.data[0], xyz.data[1], xyz.data[2], w
	};
	return ret;
}

inline float float3_elem(struct float3 v, size_t idx) {
	return v.data[idx];
}

inline void float3_set(struct float3* v, size_t idx, float f) {
	v->data[idx] = f;
}

inline float float4_elem(struct float4 v, size_t idx) {
	return v.data[idx];
}

inline void float4_set(struct float4* v, size_t idx, float f) {
	v->data[idx] = f;
}

inline float* float3_data(struct float3* v) {
	return (float*)v;
}

inline float* float4_data(struct float4* v) {
	return (float*)v;
}

inline float* float4x4_data(struct float4x4* v) {
	return (float*)v;
}

inline float dot3(struct float3 a, struct float3 b) {
	return a.data[0] * b.data[0] + a.data[1] * b.data[1] + a.data[2] * b.data[2];
}

inline float dot4(struct float4 a, struct float4 b) {
	return a.data[0] * b.data[0] + a.data[1] * b.data[1] + a.data[2] * b.data[2] + a.data[3] * b.data[3];
}

inline struct float3 cross(struct float3 a, struct float3 b) {
	return float3(
		float3_elem(a, 1) * float3_elem(b, 2) - float3_elem(a, 2) * float3_elem(b, 1),
		float3_elem(a, 2) * float3_elem(b, 0) - float3_elem(a, 0) * float3_elem(b, 2),
		float3_elem(a, 0) * float3_elem(b, 1) - float3_elem(a, 1) * float3_elem(b, 0));
}

inline struct float3 float3_normalize(struct float3 v) {
	float inv_mag = 1.0f / sqrtf(dot3(v, v));
	v.data[0] *= inv_mag;
	v.data[1] *= inv_mag;
	v.data[2] *= inv_mag;
	return v;
}

inline struct float4 float4_normalize(struct float4 v) {
	float inv_mag = 1.0f / sqrtf(dot4(v, v));
	v.data[0] *= inv_mag;
	v.data[1] *= inv_mag;
	v.data[2] *= inv_mag;
	v.data[3] *= inv_mag;
	return v;
}

inline float float4x4_elem(struct float4x4 m, size_t rowIdx, size_t colIdx) {
	return m.columns[colIdx].data[rowIdx];
}

inline void float4x4_set(struct float4x4* m, size_t row_idx, size_t col_idx, float f) {
	m->columns[col_idx].data[row_idx] = f;
}

inline struct float4 float4x4_row(struct float4x4 m, size_t idx) {
	return float4(
		float4x4_elem(m, idx, 0),
		float4x4_elem(m, idx, 1),
		float4x4_elem(m, idx, 2),
		float4x4_elem(m, idx, 3));
}

inline struct float4 float4x4_column(struct float4x4 m, size_t idx) {
	return m.columns[idx];
}

inline struct float4 float4x4_transform_float4 (struct float4x4 m, struct float4 v) {
	return float4(
		dot4(float4x4_row(m, 0), v),
		dot4(float4x4_row(m, 1), v),
		dot4(float4x4_row(m, 2), v),
		dot4(float4x4_row(m, 3), v));
}

inline struct float3 float4x4_transform_point(struct float4x4 m, struct float3 v) {
	struct float4 vw = float4x4_transform_float4(m, float4_from_float3(v, 1.0f));
	float w = vw.data[3];
	struct float3 ret = { vw.data[0] / w, vw.data[1] / w, vw.data[2] / w };
	return ret;
}

inline struct float3 float4x4_transform_vector(struct float4x4 m, struct float3 v) {
	struct float4 vw = float4x4_transform_float4(m, float4_from_float3(v, 0.0f));
	float w = vw.data[3];
	struct float3 ret = { vw.data[0], vw.data[1], vw.data[2] };
	return ret;
}

inline struct float4x4 float4x4_from_rows(struct float4 r0, struct float4 r1, struct float4 r2, struct float4 r3) {
	struct float4x4 ret = { {
		{ r0.data[0], r1.data[0], r2.data[0], r3.data[0] },
		{ r0.data[1], r1.data[1], r2.data[1], r3.data[1] },
		{ r0.data[2], r1.data[2], r2.data[2], r3.data[2] },
		{ r0.data[3], r1.data[3], r2.data[3], r3.data[3] },
	} };
	return ret;
}

inline struct float4x4 float4x4_from_columns(struct float4 c0, struct float4 c1, struct float4 c2, struct float4 c3) {
	struct float4x4 ret = {
		{ c0, c1, c2, c3 }
	};
	return ret;
}

inline struct float4x4 float4x4_identity() {
	return float4x4_from_columns(float4_unit_x(), float4_unit_y(), float4_unit_z(), float4_unit_w());
}

inline struct float4x4 float4x4_zero() {
	struct float4x4 ret = {0};
	return ret;
}

inline struct float4x4 float4x4_uniform_scale(float scale) {
	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, scale);
	float4x4_set(&ret, 1, 1, scale);
	float4x4_set(&ret, 2, 2, scale);
	return ret;
}

inline struct float4x4 float4x4_nonuniform_scale(float sx, float sy, float sz) {
	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, sx);
	float4x4_set(&ret, 1, 1, sy);
	float4x4_set(&ret, 2, 2, sz);
	return ret;
}

inline struct float4x4 float4x4_nonuniform_scale_float3(struct float3 scales) {
	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, float3_elem(scales, 0));
	float4x4_set(&ret, 1, 1, float3_elem(scales, 1));
	float4x4_set(&ret, 2, 2, float3_elem(scales, 2));
	return ret;
}

inline struct float4x4 float4x4_translate(float dx, float dy, float dz) {
	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 3, dx);
	float4x4_set(&ret, 1, 3, dy);
	float4x4_set(&ret, 2, 3, dz);
	return ret;
}

inline struct float4x4 float4x4_translate_float3(struct float3 v) {
	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 3, float3_elem(v, 0));
	float4x4_set(&ret, 1, 3, float3_elem(v, 1));
	float4x4_set(&ret, 2, 3, float3_elem(v, 2));
	return ret;
}

inline struct float4x4 float4x4_rotate_x(float angle) {
	float c = cosf(angle);
	float s = sinf(angle);

	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 1, 1, +c); float4x4_set(&ret, 1, 2, -s);
	float4x4_set(&ret, 2, 1, +s); float4x4_set(&ret, 2, 2, +c);
	return ret;
}

inline struct float4x4 float4x4_rotate_y(float angle) {
	float c = cosf(angle);
	float s = sinf(angle);

	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, +c); float4x4_set(&ret, 0, 2, +s);
	float4x4_set(&ret, 2, 0, -s); float4x4_set(&ret, 2, 2, +c);
	return ret;
}

inline struct float4x4 float4x4_rotate_z(float angle) {
	float c = cosf(angle);
	float s = sinf(angle);

	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, +c); float4x4_set(&ret, 0, 1, -s);
	float4x4_set(&ret, 1, 0, +s); float4x4_set(&ret, 1, 1, +c);
	return ret;
}

inline struct float4x4 float4x4_rotate_axis_angle(struct float3 axis, float angle) {
	float c = cosf(angle);
	float ic = 1.0f - c;
	float s = sinf(angle);
	float ux = float3_elem(axis, 0);
	float uy = float3_elem(axis, 1);
	float uz = float3_elem(axis, 2);

	struct float4x4 ret = float4x4_identity();
	float4x4_set(&ret, 0, 0, c + ux*ux * ic);
	float4x4_set(&ret, 0, 1, ux*uy * ic - uz * s);
	float4x4_set(&ret, 0, 2, ux*uz * ic + uy * s);

	float4x4_set(&ret, 1, 0, uy*ux * ic + uz * s);
	float4x4_set(&ret, 1, 1, c + uy*uy * ic);
	float4x4_set(&ret, 1, 2, uy*uz * ic - ux * s);

	float4x4_set(&ret, 2, 0, uz*ux * ic - uy * s);
	float4x4_set(&ret, 2, 1, uz*uy * ic + ux * s);
	float4x4_set(&ret, 2, 2, c + uz*uz * ic);

	return ret;
}

inline struct float4x4 float4x4_perspective_d3d_rh(float nearZ, float farZ, float width, float height) {
	struct float4x4 ret = float4x4_zero();
	float4x4_set(&ret, 0, 0, 2.0f * nearZ / width);
	
	float4x4_set(&ret, 1, 1, 2.0f * nearZ / height);

	float4x4_set(&ret, 2, 2, farZ / (nearZ - farZ));
	float4x4_set(&ret, 2, 3, nearZ * farZ / (nearZ - farZ));
	
	float4x4_set(&ret, 3, 2, -1.0f);
	return ret;
}

inline struct float4x4 float4x4_perspective_gl_rh(float nearZ, float farZ, float width, float height) {
	struct float4x4 ret = float4x4_zero();
	float4x4_set(&ret, 0, 0, 2.0f * nearZ / width);

	float4x4_set(&ret, 1, 1, 2.0f * nearZ / height);

	float4x4_set(&ret, 2, 2, (nearZ + farZ) / (nearZ - farZ));
	float4x4_set(&ret, 2, 3, 2.0f * nearZ * farZ / (nearZ - farZ));

	float4x4_set(&ret, 3, 2, -1.0f);
	return ret;
}

inline struct float4x4 float4x4_mul(struct float4x4 a, struct float4x4 b) {
	return float4x4_from_columns(
		float4x4_transform_float4(a, float4x4_column(b, 0)),
		float4x4_transform_float4(a, float4x4_column(b, 1)),
		float4x4_transform_float4(a, float4x4_column(b, 2)),
		float4x4_transform_float4(a, float4x4_column(b, 3)));
}

#ifdef __cplusplus
}
#endif
