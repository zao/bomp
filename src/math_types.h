#pragma once

#ifdef __cplusplus
extern "C" {
#endif

struct float2 {
	float data[2];
};

struct float3 {
	float data[3];
};

struct float4 {
	float data[4];
};

struct float4x4 {
	struct float4 columns[4];
};

#ifdef __cplusplus
}
#endif
