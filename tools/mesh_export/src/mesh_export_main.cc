#include <fbxsdk.h>
#include <Windows.h>

#include <array>
#include <functional>
#include <map>
#include <vector>

#include <assert.h>

FbxManager* manager{};

bool ConvertScene(char const* srcFilename, char const* dstFilename) {
	FbxScene* scene = FbxScene::Create(manager, "");

	FbxImporter* importer = FbxImporter::Create(manager, "");
	bool imported = importer->Initialize(srcFilename, -1, manager->GetIOSettings());
	if (!imported) {
		auto err = importer->GetStatus().GetErrorString();
		printf(err);
		OutputDebugStringA(err);
	}
	importer->Import(scene);
	importer->Destroy();

	struct VertexPNBiBwU {
		std::array<float, 4> position;
		std::array<float, 3> normal;
		float pad0;
		std::array<int32_t, 4> blendIndices{-1,-1,-1,-1};
		std::array<float, 4> blendWeights;
		std::array<float, 2> texcoord;
		float pad1[2];
	};

	std::vector<VertexPNBiBwU> outVertices;
	{
		FbxGeometryConverter cvt(manager);
		cvt.Triangulate(scene, true);
	}

	struct Skeleton {
		struct Joint {
			uint32_t parentIndex{};
			FbxAMatrix invBind{};
		};
		std::vector<Joint> joints;
		std::vector<FbxNode*> nodes;
	};

	Skeleton skeleton;

	struct Animation {
		struct Keyframe {
			FbxLongLong frameNumber;
			FbxAMatrix boneMatrix;
		};
		struct Frameset {
			std::vector<Keyframe> keyframes;
		};
		std::vector<Frameset> framesets;
	};

	Animation animation;

	auto* root = scene->GetRootNode();

	std::function<void (FbxNode*, int, int, int)> visitSkeletonNode;
	visitSkeletonNode = [&visitSkeletonNode, &skeleton](FbxNode* node, int indent, int jointIndex, int parentIndex) {
		FbxNodeAttribute* attrib = node->GetNodeAttribute();
		if (attrib && attrib->GetAttributeType() == FbxNodeAttribute::eSkeleton) {
			Skeleton::Joint joint{};
			joint.parentIndex = parentIndex;
			skeleton.joints.push_back(std::move(joint));
			skeleton.nodes.push_back(node);
		}
		int childCount = node->GetChildCount();
		for (int childIdx = 0; childIdx < childCount; ++childIdx) {
			visitSkeletonNode(node->GetChild(childIdx), indent + 1, static_cast<int>(skeleton.joints.size()), jointIndex);
		}
	};
	visitSkeletonNode(root, 0, 0, -1);

	std::function<void (FbxNode*, int)> visitGeometryNode;
	visitGeometryNode = [scene, &visitGeometryNode, &outVertices, &skeleton, &animation](FbxNode* node, int indent) {
		auto pointXform = node->EvaluateGlobalTransform();
		auto normalXform = pointXform.Inverse().Transpose();
		for (size_t i = 0; i < indent; ++i) {
			printf("  ");
		}
		FbxMesh* mesh = node->GetMesh();
		printf("%s (%s)\n", node->GetName(), mesh ? "has mesh" : "no mesh");

		if (mesh) {
			int currentVertex = 0;
			bool hasNormals = mesh->GetElementNormalCount() > 0;
			bool hasUVs = mesh->GetElementUVCount() > 0;
			auto triCount = mesh->GetPolygonCount();
			for (int poly = 0; poly < triCount; ++poly) {
				VertexPNBiBwU p[3];
				for (int i = 0; i < 3; ++i) {
					int idx = mesh->GetPolygonVertex(poly, i);
					auto cpPos = mesh->GetControlPointAt(idx);
					VertexPNBiBwU outVtx{};
					auto xPos = pointXform.MultT(cpPos);
					outVtx.position = {
						static_cast<float>(xPos.mData[0]),
						static_cast<float>(xPos.mData[1]),
						static_cast<float>(xPos.mData[2]),
						1.0f
					};

					// printf("p = [ %f %f %f ]';\n", xPos.mData[0], xPos.mData[1], xPos.mData[2]);
					if (hasNormals) {
						FbxGeometryElementNormal* normals = mesh->GetElementNormal();
						auto mappingMode = normals->GetMappingMode();
						auto referenceMode = normals->GetReferenceMode();
						FbxVector4 cpNormal{};
						int cp{};
						if (mappingMode == FbxGeometryElement::eByControlPoint) {
							cp = idx;
						}
						if (mappingMode == FbxGeometryElement::eByPolygonVertex) {
							cp = currentVertex;
						}
						if (referenceMode == FbxGeometryElement::eIndexToDirect) {
							cp = normals->GetIndexArray().GetAt(cp);
						}
						cpNormal = normals->GetDirectArray().GetAt(cp);
						auto xNormal = normalXform.MultT(FbxVector4(cpNormal.mData[0], cpNormal.mData[1], cpNormal.mData[2], 0.0f));
						xNormal.Normalize();
						outVtx.normal = {
							static_cast<float>(xNormal.mData[0]),
							static_cast<float>(xNormal.mData[1]),
							static_cast<float>(xNormal.mData[2]),
						};
						// printf("n = [ %f %f %f ]';\n", cpNormal.mData[0], cpNormal.mData[1], cpNormal.mData[2]);
					}

					if (hasUVs) {
						FbxGeometryElementUV* uvs = mesh->GetElementUV();
						auto mappingMode = uvs->GetMappingMode();
						auto referenceMode = uvs->GetReferenceMode();
						FbxVector2 cpUV{};
						int cp{};
						if (mappingMode == FbxGeometryElement::eByControlPoint) {
							cp = idx;
						}
						if (mappingMode == FbxGeometryElement::eByPolygonVertex) {
							cp = currentVertex;
						}
						if (referenceMode == FbxGeometryElement::eIndexToDirect) {
							cp = uvs->GetIndexArray().GetAt(cp);
						}
						cpUV = uvs->GetDirectArray().GetAt(cp);
						outVtx.texcoord = {
							static_cast<float>(cpUV.mData[0]),
							static_cast<float>(cpUV.mData[1]),
						};
						// printf("t = [ %f %f ]';\n", cpUV.mData[0], cpUV.mData[1]);
					}
					switch (i) {
					case 0: p[0] = outVtx; break;
					case 1: p[2] = outVtx; break;
					case 2: p[1] = outVtx; break;
					}
					++currentVertex;
				}
				outVertices.insert(outVertices.end(), p + 0, p + 3);
			}
			int deformerCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
			printf("%d deformers\n", deformerCount);
			assert(deformerCount <= 1);
			for (int deformerIdx = 0; deformerIdx < deformerCount; ++deformerIdx) {
				FbxDeformer* dform = mesh->GetDeformer(deformerIdx, FbxDeformer::eSkin);
				FbxSkin* skin = static_cast<FbxSkin*>(dform);
				int clusterCount = skin->GetClusterCount();
				for (int clusterIdx = 0; clusterIdx < clusterCount; ++clusterIdx) {
					FbxCluster* cluster = skin->GetCluster(clusterIdx);
					FbxNode* link = cluster->GetLink();
					std::string jointName = cluster->GetLink()->GetName();
					auto I = std::find(skeleton.nodes.begin(), skeleton.nodes.end(), link);
					int jointIndex = static_cast<int>(std::distance(skeleton.nodes.begin(), I));
					FbxAMatrix transform, linkTransform;
					cluster->GetTransformMatrix(transform);
					cluster->GetTransformLinkMatrix(linkTransform);
					FbxAMatrix globalBindPoseInverse = linkTransform.Inverse() * transform;
					skeleton.joints[jointIndex].invBind = globalBindPoseInverse;

					int numIndices = cluster->GetControlPointIndicesCount();
					for (int i = 0; i < numIndices; ++i) {
						int index = cluster->GetControlPointIndices()[i];
						double weight = cluster->GetControlPointWeights()[i];
						auto& outVtx = outVertices[index];
						for (int j = 0; j < 4; ++j) {
							if (outVtx.blendIndices[j] == -1) {
								outVtx.blendIndices[j] = jointIndex;
								outVtx.blendWeights[j] = static_cast<float>(weight);
								break;
							}
						}
					}

					FbxAnimStack* animStack = scene->GetSrcObject<FbxAnimStack>(0);
					FbxString animStackName = animStack->GetName();
					FbxTakeInfo* takeInfo = scene->GetTakeInfo(animStackName);
					FbxTime start = takeInfo->mLocalTimeSpan.GetStart();
					FbxTime end = takeInfo->mLocalTimeSpan.GetStop();
					FbxLongLong startCount = start.GetFrameCount(FbxTime::eFrames60);
					FbxLongLong endCount = end.GetFrameCount(FbxTime::eFrames60);
					FbxLongLong animationLength = endCount - startCount + 1;

					for (FbxLongLong i = startCount; i <= endCount; ++i) {
						FbxTime t;
						t.SetFrame(i, FbxTime::eFrames60);
						FbxAMatrix transformOffset = node->EvaluateGlobalTransform(t);
						Animation::Keyframe kf{};
						kf.frameNumber = i;
						kf.boneMatrix = transformOffset.Inverse() * link->EvaluateGlobalTransform(t);
						animation.framesets.resize(i);
						auto& frameset = animation.framesets[i];
						frameset.keyframes.resize(skeleton.joints.size());
						frameset.keyframes[jointIndex] = kf;
					}
				}
			}
		}

		int n = node->GetChildCount();
		for (int i = 0; i < n; ++i) {
			visitGeometryNode(node->GetChild(i), indent + 1);
		}
	};
	visitGeometryNode(root, 0);

	for (auto& vtx : outVertices) {
		for (int i = 0; i < 4; ++i) {
			if (vtx.blendIndices[i] == -1) {
				vtx.blendIndices[i] = 0;
				vtx.blendWeights[i] = 0.0f;
			}
		}
	}

	FILE* fh{};
	fopen_s(&fh, dstFilename, "wb");
	uint32_t vertexCount = static_cast<uint32_t>(outVertices.size());
	fwrite(&vertexCount, sizeof(uint32_t), 1, fh);
	fwrite(outVertices.data(), sizeof(VertexPNBiBwU), outVertices.size(), fh);
#if 0
	uint32_t jointCount = static_cast<uint32_t>(skeleton.joints.size());
	fwrite(&jointCount, sizeof(uint32_t), 1, fh);
	fwrite(skeleton.joints.data(), sizeof(Skeleton::Joint), skeleton.joints.size(), fh);

	uint32_t frameCount = static_cast<uint32_t>(animation.framesets.size());
	fwrite(&frameCount, sizeof(uint32_t), 1, fh);
	for (auto& fs : animation.framesets) {
		for (auto& kf : fs.keyframes) {
			fwrite(&kf.frameNumber, sizeof(FbxLongLong), 1, fh);
			fwrite(&kf.boneMatrix, sizeof(FbxAMatrix), 1, fh);
		}
	}
#endif
	fclose(fh);

	return true;
}

int main() {
	::manager = FbxManager::Create();
	FbxIOSettings* ios = FbxIOSettings::Create(manager, IOSROOT);
	manager->SetIOSettings(ios);

	ConvertScene("C:/dev/bomp/assets/meshes/human.fbx", "C:/dev/bomp/assets/meshes/human.lv_mesh");
	ConvertScene("C:/dev/bomp/assets/meshes/spruce.fbx", "C:/dev/bomp/assets/meshes/spruce.lv_mesh");

	manager->Destroy();
}